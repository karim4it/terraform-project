provider "aws" {
  region     = "us-east-1"
  access_key = "AKIAXC2T3MZERPDZESTR"
  secret_key = "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"
}

resource "aws_instance" "terraform_instance" {
  ami           = "ami-0742b4e673072066f"
  instance_type = "t2.micro"

  tags = {
    Name = "HelloKarim"
  }
}
resource "aws_vpc" "Aiza" {
  cidr_block = "6.0.0.0/16"
  tags = {
    Name = "1stvpc"
  }
}

resource "aws_subnet" "Aiza_subnet" {
  vpc_id     = aws_vpc.Aiza.id
  cidr_block = "6.0.0.0/16"

  tags = {
    Name = "Aiza_subnet"
  }
}
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.Aiza.id

  tags = {
    Name = "aiza_IG"
  }
}
resource "aws_route_table" "aiza_route_table" {
  vpc_id = aws_vpc.Aiza.id

  route {
    cidr_block = "6.0.0.0/16"
    gateway_id = aws_internet_gateway.gw.id
  }

  route {
    ipv6_cidr_block        = "::/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "Prod"
  }
}
resource "aws_subnet" Aiza2_subnet {
  vpc_id = aws_vpc.Aiza.id
  cidr_block = "0.0.0.0/16"
  availability_zone = "us-east-1a"

}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet-1.id
  route_table_id = aws_route_table.aiza_route_table.id
}

resource "aws_security_group" "allow_web" {
  name        = "allow_web_traffic"
  description = "Allow web inbound traffic"
  vpc_id      = aws_vpc.Aiza.id

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
   
  }
ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }
  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
   
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    }

    tags = { 
      Name = "allow_web"
    }
}


resource "aws_network_interface" "web-server-nic" {
  subnet_id       = aws_subnet.Aiza_subnet.id
  private_ips     = ["6.0.1.50"]
  security_groups = [aws_security_group.allow_web.id]

}
resource "aws_eip" "one" {
  vpc                       = true
  network_interface         = aws_network_interface.web-server-nic.id
  associate_with_private_ip = "6.0.1.50"
depends_on = [aws_internet_gateway.gw]

}

resource "aws_instance" "terraform_instance2" {
ami           = "ami-0742b4e673072066f"
  instance_type = "t2.micro"
  availability_zone ="us-east-1a"
  network_interface {
  device_index = 0 
  network_interface_id = aws_network_interface.web-server-nic.id
}

  user_data = <<-EOF
            #!/bin/bash
            sudo apt update -y
            sudo apt install apache2 -y
            sudo systemctl start apache2
            sudo bash -c 'echo your first web server via terraform > /var/www/html/index.html'
            EOF
  tags = {
    Name = "web-server"
  }
}